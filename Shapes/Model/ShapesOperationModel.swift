//
//  ShapesViewModel.swift
//  Shapes
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

typealias ShapeIdentifier = UUID

enum ShapeOperationType {
    case create
    case delete
    case move
}

enum Shape: String {
    case circle = "Circle"
    case square = "Square"
    case triangle = "Triangle"
}

struct ShapeOperation {
    let operationType: ShapeOperationType
    let shapeModel: ShapeModel
}

struct ShapeModel {
    let shapeID: ShapeIdentifier
    let frame: CGRect
    let shape: Shape
    
    init(frame: CGRect, shape: Shape) {
        self.shapeID = ShapeIdentifier()
        self.frame = frame
        self.shape = shape
    }
    
    private init (shapeID: ShapeIdentifier, frame: CGRect, shape: Shape) {
        self.shapeID = shapeID
        self.frame = frame
        self.shape = shape
    }
    
    fileprivate func move(origin: CGPoint) -> ShapeModel {
        let updatedFrame = CGRect(x: origin.x, y: origin.y, width: frame.width, height: frame.height)
        return ShapeModel(shapeID: shapeID, frame: updatedFrame, shape: shape)
    }
}

protocol ShapesOperationModelable {
    var shapeModels: [ShapeModel] {get}
    var shapeModelsUpdated: (() -> ())? {get set}
    func update(canvasBounds: CGRect)
    func create(shape: Shape)
    func move(shapeModel: ShapeModel, toOrigin origin: CGPoint)
    func deleteAll(shape: Shape)
    func delete(shapeModel: ShapeModel)
    func undoLastOperation()
    func operationCount() -> Int
}

class ShapesOperationModel: ShapesOperationModelable {
    
    private var shapeOperations = [ShapeOperation]() {
        didSet {
            shapeModels = generateShapeModels(shapeOperations: shapeOperations)
        }
    }
    var shapeModelsUpdated: (() -> ())?
    var shapeModels = [ShapeModel]() {
        didSet {
            shapeModelsUpdated?()
        }
    }
    private var shapeCanvasBounds: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    private let shapeSize: CGSize
    
    init(shapeSize: CGSize) {
        self.shapeSize = shapeSize
    }
    
    func update(canvasBounds: CGRect) {
        shapeCanvasBounds = canvasBounds
    }
    
    func create(shape: Shape) {
        let shapeFrame = randomFrame(maximumBounds: shapeCanvasBounds, shapeSize: shapeSize)
        let shapeModel = ShapeModel(frame: shapeFrame, shape: shape)
        shapeOperations.append(ShapeOperation(operationType: .create, shapeModel: shapeModel))
    }
    
    func move(shapeModel: ShapeModel, toOrigin origin: CGPoint) {
        let updatedShapeModel = shapeModel.move(origin: origin)
        shapeOperations.append(ShapeOperation(operationType: .move, shapeModel: updatedShapeModel))
    }
    
    func undoLastOperation() {
        shapeOperations.removeLast()
    }
    
    func operationCount() -> Int {
        return shapeOperations.count
    }
    
    func deleteAll(shape: Shape) {
        let shapeModelsForDeletion = shapeModels.filter { $0.shape == shape }
        for shapeModel in shapeModelsForDeletion {
            shapeOperations.append(ShapeOperation(operationType: .delete, shapeModel: shapeModel))
        }
    }
    
    func delete(shapeModel: ShapeModel) {
        guard shapeModels.contains(where: { shapeModel.shapeID == $0.shapeID }) else {
            return
        }
        shapeOperations.append(ShapeOperation(operationType: .delete, shapeModel: shapeModel))
    }
    
    private func randomFrame(maximumBounds: CGRect, shapeSize: CGSize) -> CGRect {
        guard maximumBounds.maxX > shapeSize.width + maximumBounds.minX, maximumBounds.maxY + maximumBounds.minY > shapeSize.height else {
            assertionFailure("Requested size was not valid for bounds")
            return CGRect(x: 0, y: 0, width: 0, height: 0)
        }
        let maxXOrigin = UInt32(maximumBounds.maxX - shapeSize.width - maximumBounds.minX)
        let maxYOrigin = UInt32(maximumBounds.maxY - shapeSize.height - maximumBounds.minY)
        let originX = CGFloat(arc4random_uniform(maxXOrigin)) + maximumBounds.minX
        let originY = CGFloat(arc4random_uniform(maxYOrigin)) + maximumBounds.minY
        return CGRect(x: originX, y: originY, width: shapeSize.width, height: shapeSize.height)
    }
    
    private func generateShapeModels(shapeOperations: [ShapeOperation]) -> [ShapeModel] {
        var shapeModels = [ShapeIdentifier: ShapeModel]()
        for shapeOperation in shapeOperations {
            switch shapeOperation.operationType {
            case .create, .move:
                shapeModels[shapeOperation.shapeModel.shapeID] = shapeOperation.shapeModel
            case .delete:
                shapeModels.removeValue(forKey: shapeOperation.shapeModel.shapeID)
            }
        }
        return shapeModels.flatMap({ (key, value) -> ShapeModel? in
            return value
        })
    }
    
}
