//
//  Square.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class Square: UIView, ShapeView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .blue
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .blue
    }

}
