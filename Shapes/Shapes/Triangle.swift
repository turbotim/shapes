//
//  Triangle.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class Triangle: UIView, ShapeView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .clear
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.closePath()
        context.setFillColor(UIColor.green.cgColor)
        context.fillPath()
    }

}
