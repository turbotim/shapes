//
//  Circle.swift
//  Shapes
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class Circle: UIView, ShapeView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
        layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .red
        layer.masksToBounds = true
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = bounds.width / 2
    }
}
