//
//  ShapesViewModel.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol ShapeViewModel {
    var shapeID: ShapeIdentifier {get}
    var frame: CGRect {get}
    var shape: Shape {get}
}

extension ShapeModel: ShapeViewModel {}

class ShapesViewModel {

    var shapesChanged: (() -> ())?
    private var shapesOperationModel: ShapesOperationModelable
    var shapeViewModels = [ShapeViewModel]() {
        didSet {
            shapesChanged?()
        }
    }
    
    init(shapesOperationModel: ShapesOperationModelable) {
        self.shapesOperationModel = shapesOperationModel
        self.shapesOperationModel.shapeModelsUpdated = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.shapeViewModels = strongSelf.shapesOperationModel.shapeModels
        }
    }
    
    func undo() {
        if displayUndo() {
            shapesOperationModel.undoLastOperation()
        }
    }
    
    func update(canvasBounds: CGRect) {
        shapesOperationModel.update(canvasBounds: canvasBounds)
    }
    
    func create(shape: Shape) {
        shapesOperationModel.create(shape: shape)
    }
    
    func moveShape(shapeID: ShapeIdentifier, origin: CGPoint) {
        let shapeViewModel = shapeViewModels.filter { $0.shapeID == shapeID }.first
        guard let shapeModel = shapeViewModel as? ShapeModel else {
            return
        }
        shapesOperationModel.move(shapeModel: shapeModel, toOrigin: origin)
    }
    
    func deleteShape(shapeID: ShapeIdentifier) {
        let shapeViewModel = shapeViewModels.filter { $0.shapeID == shapeID }.first
        guard let shapeModel = shapeViewModel as? ShapeModel else {
            return
        }
        shapesOperationModel.delete(shapeModel: shapeModel)
    }
    
    func isDisplayedShape(shapeID: ShapeIdentifier) -> Bool {
        return shapeViewModels.contains { $0.shapeID == shapeID }
    }
    
    func displayUndo() -> Bool {
        return shapesOperationModel.operationCount() > 0
    }
}
