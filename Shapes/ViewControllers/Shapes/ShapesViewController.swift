//
//  ViewController.swift
//  Shapes
//
//  Created by Tim Harrison on 01/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class ShapesViewController: UIViewController {

    private let circleButton = UIButton(type: .system)
    private let squareButton = UIButton(type: .system)
    private let triangleButton = UIButton(type: .system)
    private let undoButton = UIButton(type: .system)
    private let statsButton = UIButton(type: .system)
    private lazy var buttonStackView: UIStackView = {UIStackView(arrangedSubviews: [circleButton, triangleButton, squareButton])}()
    private var displayedShapes = [ShapeIdentifier: UIView]()
    private var movingView: (UIView)? = nil
    private var viewModel: ShapesViewModel! = nil
    private var router: ShapesRouter! = nil
    
    override func loadView() {
        super.loadView()
        view.addSubview(buttonStackView)
        installConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(panGestureRecognizer:)))
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPreseed(longPressRecognizer:)))
        view.addGestureRecognizer(longPressRecognizer)
        view.addGestureRecognizer(panGesture)
        buttonStackView.distribution = .fillEqually
        viewModel.shapesChanged = { [weak self] in
            self?.updateShapes()
        }
        title = "Shapes"
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: statsButton)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: undoButton)
        configureButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let navigationMaxY = navigationController?.navigationBar.frame.maxY ?? 0
        let totalBorderSize = buttonStackView.frame.height + navigationMaxY
        let borderedBounds = CGRect(x: 0, y: navigationMaxY, width: view.bounds.width, height: view.bounds.height - totalBorderSize)
        viewModel.update(canvasBounds: borderedBounds)
    }
    
    private func configure() {
        //Because this view controller is the app entry point we have to wire these up here
        let shapesOperationModel = ShapesOperationModel(shapeSize: CGSize(width: 100, height: 100))
        viewModel = ShapesViewModel(shapesOperationModel: shapesOperationModel)
        router = ShapesRouter(navigationController: navigationController, shapesOperationModel: shapesOperationModel)
    }
    
    @objc func longPreseed(longPressRecognizer: UILongPressGestureRecognizer) {
        let location = longPressRecognizer.location(in: view)
        guard let touchedShape = view.hitTest(location, with: nil) as? ShapeView & UIView,
            let shapeID = shapeID(shapeView: touchedShape) else {
                return
        }
        viewModel.deleteShape(shapeID: shapeID)
    }
    
    @objc func draggedView(panGestureRecognizer: UIPanGestureRecognizer){
        
        let location = panGestureRecognizer.location(in: view)
        
        switch panGestureRecognizer.state {
        case .began:
            if let movingView = view.hitTest(location, with: nil) as? ShapeView & UIView {
                self.movingView = movingView
            }
        case .cancelled, .ended:
            if let movingView = self.movingView, let shapeID = shapeID(shapeView: movingView) {
                viewModel.moveShape(shapeID: shapeID, origin: movingView.frame.origin)
            }
            movingView = nil
        case .changed:
            guard let movingView = self.movingView else {
                return
            }
            let translation = panGestureRecognizer.translation(in: view)
            movingView.center = CGPoint(x: movingView.center.x + translation.x, y: movingView.center.y + translation.y)
            panGestureRecognizer.setTranslation(CGPoint.zero, in: view)
        case .failed, .possible:
            break
        }
    }
    
    private func updateShapes() {
        for shapeViewModel in viewModel.shapeViewModels {
            if let existingView = displayedShapes[shapeViewModel.shapeID] {
                existingView.frame = shapeViewModel.frame
            } else {
                let shape = shapeView(shape: shapeViewModel.shape, rect: shapeViewModel.frame)
                view.addSubview(shape)
                displayedShapes[shapeViewModel.shapeID] = shape
            }
        }
        
        let deletedShapes = displayedShapes.filter { !viewModel.isDisplayedShape(shapeID: $0.key) }
        for deletedShape in deletedShapes {
            deletedShape.value.removeFromSuperview()
            displayedShapes.removeValue(forKey: deletedShape.key)
        }
        undoButton.isEnabled = viewModel.displayUndo()
    }
    
    private func shapeView(shape: Shape, rect: CGRect) -> UIView {
        switch shape {
        case .circle:
            return Circle(frame: rect)
        case .square:
            return Square(frame: rect)
        case .triangle:
            return Triangle(frame: rect)
        }
    }

    private func configureButtons() {
        circleButton.setTitle("Circle", for: .normal)
        circleButton.addTarget(self, action: #selector(addShapeButtonTapped(button:)), for: .touchUpInside)
        squareButton.setTitle("Square", for: .normal)
        squareButton.addTarget(self, action: #selector(addShapeButtonTapped(button:)), for: .touchUpInside)
        triangleButton.setTitle("Triangle", for: .normal)
        triangleButton.addTarget(self, action: #selector(addShapeButtonTapped(button:)), for: .touchUpInside)
        undoButton.setTitle("Undo", for: .normal)
        undoButton.addTarget(self, action: #selector(undoButtonTapped), for: .touchUpInside)
        undoButton.isEnabled = false
        statsButton.setTitle("Stats", for: .normal)
        statsButton.addTarget(self, action: #selector(statsButtonTapped), for: .touchUpInside)
    }
    
    private func installConstraints() {
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        undoButton.translatesAutoresizingMaskIntoConstraints = false
        statsButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            buttonStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            buttonStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            buttonStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            buttonStackView.heightAnchor.constraint(equalToConstant: 44)
            ])
    }
    
    private func shapeID(shapeView: UIView) -> ShapeIdentifier? {
        return displayedShapes.filter { $0.value == shapeView }.first?.key
    }
    
    @objc private func statsButtonTapped() {
        router.showStats()
    }
    
    @objc private  func undoButtonTapped() {
        viewModel.undo()
    }
    
    @objc private func addShapeButtonTapped(button: UIButton) {
        if button == circleButton {
            viewModel.create(shape: .circle)
        } else if button == squareButton {
            viewModel.create(shape: .square)
        } else if button == triangleButton {
            viewModel.create(shape: .triangle)
        }
    }
    
}

