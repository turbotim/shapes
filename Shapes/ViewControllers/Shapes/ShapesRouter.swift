//
//  ShapesRouter.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class ShapesRouter {

    let navigationController: UINavigationController?
    let shapesOperationModel: ShapesOperationModelable
    
    init(navigationController: UINavigationController?, shapesOperationModel: ShapesOperationModel) {
        self.navigationController = navigationController
        self.shapesOperationModel = shapesOperationModel
    }
    
    func showStats() {
        let statsViewModel = StatsViewModel(shapesOperationModel: shapesOperationModel)
        let statsViewController = StatsViewController(viewModel: statsViewModel)
        navigationController?.pushViewController(statsViewController, animated: true)
    }
}
