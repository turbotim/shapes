//
//  StatsViewModel.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

protocol StatsCellViewModelable {
    var statText: String {get}
}

struct StatsCellViewModel: StatsCellViewModelable {
    var statText: String {
        if count == 1 {
            return "\(count) \(shape.rawValue)"
        } else {
            return "\(count) \(shape.rawValue)s"
        }
    }
    let shape: Shape
    var count: Int = 0
    
    init(shape: Shape) {
        self.shape = shape
    }
}

class StatsViewModel: NSObject {

    private let shapesOperationModel: ShapesOperationModelable
    private var statsCellViewModels = [StatsCellViewModel]() {
        didSet {
            statsChanged?()
        }
    }
    
    var statsChanged: (() -> ())? = nil
    
    init(shapesOperationModel: ShapesOperationModelable) {
        self.shapesOperationModel = shapesOperationModel
    }
    
    func fetchStats() {
        var triangleViewModel = StatsCellViewModel(shape: .triangle)
        var circleViewModel = StatsCellViewModel(shape: .circle)
        var squareViewModel = StatsCellViewModel(shape: .square)
        
        for shapeModel in shapesOperationModel.shapeModels {
            switch shapeModel.shape {
            case .circle:
                circleViewModel.count = circleViewModel.count + 1
            case .square:
                squareViewModel.count = squareViewModel.count + 1
            case .triangle:
                triangleViewModel.count = triangleViewModel.count + 1
            }
        }

        let allCellViewModels = [triangleViewModel, circleViewModel, squareViewModel]
        statsCellViewModels = allCellViewModels.filter { $0.count > 0 }
    }
    
    func cellCount() -> Int {
        return statsCellViewModels.count
    }
    
    func statsViewModel(indexPath: IndexPath) -> StatsCellViewModel? {
        guard indexPath.item < statsCellViewModels.count else {
            return nil
        }
        return statsCellViewModels[indexPath.item]
    }
    
    func deleteCell(indexPath: IndexPath) {
        guard indexPath.item < statsCellViewModels.count, let cellViewModel = statsViewModel(indexPath: indexPath) else {
            return
        }
        
        shapesOperationModel.deleteAll(shape: cellViewModel.shape)
        fetchStats()
    }
    
}
