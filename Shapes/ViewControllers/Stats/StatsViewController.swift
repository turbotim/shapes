//
//  StatsViewController.swift
//  Shapes
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class StatsViewController: UITableViewController {
    
    let viewModel: StatsViewModel
    let statsCellReuseIdentifier = "StatsCellReuseIdentifier"
    
    init(viewModel: StatsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: statsCellReuseIdentifier)
        tableView.allowsSelection = false
        viewModel.statsChanged = { [weak self] in
            self?.tableView.reloadData()
        }
        viewModel.fetchStats()
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellCount()
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: statsCellReuseIdentifier, for: indexPath)
        guard let statsCellViewModel = viewModel.statsViewModel(indexPath: indexPath) else {
            return cell
        }
        cell.textLabel?.text = statsCellViewModel.statText
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "delete") { [weak self] (action, indexPath) in
            self?.viewModel.deleteCell(indexPath: indexPath)
        }
        return [delete]
    }
}
