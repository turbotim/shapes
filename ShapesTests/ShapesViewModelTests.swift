//
//  ShapesViewModelTests.swift
//  ShapesTests
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import Shapes

class ShapesViewModelTests: XCTestCase {
    
    var shapesOperationModelMock = ShapesOperationModelMock()
    var shapesViewModel = ShapesViewModel(shapesOperationModel: ShapesOperationModelMock())
    
    override func setUp() {
        super.setUp()
        shapesOperationModelMock = ShapesOperationModelMock()
        shapesViewModel = ShapesViewModel(shapesOperationModel: shapesOperationModelMock)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNotifiesOnChangesOfShapeModel() {
        let expectation = XCTestExpectation(description: "View model should get updates from model")
        shapesViewModel.shapesChanged = {
            expectation.fulfill()
        }
        shapesOperationModelMock.changeModel()
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testDisplaysUndo() {
        shapesOperationModelMock.rawOperationCount = 1
        XCTAssert(shapesViewModel.displayUndo())
        shapesOperationModelMock.rawOperationCount = 0
        XCTAssertFalse(shapesViewModel.displayUndo())
    }
}
