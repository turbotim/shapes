//
//  StatsViewModelTests.swift
//  ShapesTests
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import Shapes

class StatsViewModelTests: XCTestCase {
    
    var shapesOperationModel = ShapesOperationModelMock()
    var statsViewModel = StatsViewModel(shapesOperationModel: ShapesOperationModelMock())
    var sampleShapeModels = [ShapeModel]()
    
    override func setUp() {
        super.setUp()
        shapesOperationModel = ShapesOperationModelMock()
        statsViewModel = StatsViewModel(shapesOperationModel: shapesOperationModel)
        let dummyFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
        sampleShapeModels = [ShapeModel(frame: dummyFrame, shape: .triangle), ShapeModel(frame: dummyFrame, shape: .triangle), ShapeModel(frame: dummyFrame, shape: .circle)]
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCountsCorrectNumberOfCells() {
        XCTAssertEqual(statsViewModel.cellCount(), 0)
        shapesOperationModel.shapeModels = sampleShapeModels
        statsViewModel.fetchStats()
        XCTAssertEqual(statsViewModel.cellCount(), 2)
    }
    
    func testStatsAreCorrectlyCalculated() {
        shapesOperationModel.shapeModels = sampleShapeModels
        statsViewModel.fetchStats()
        for item in 0..<statsViewModel.cellCount() {
            guard let cellViewModel = statsViewModel.statsViewModel(indexPath: IndexPath(item: item, section: 0)) else {
                XCTFail()
                return
            }
            switch cellViewModel.shape {
            case .circle:
                XCTAssertEqual(cellViewModel.statText, "1 Circle")
            case .square:
                XCTFail()
            case .triangle:
                XCTAssertEqual(cellViewModel.statText, "2 Triangles")
            }
        }
    }
    
    func testStatsCanBeDeleted() {
        shapesOperationModel.shapeModels = sampleShapeModels
        statsViewModel.fetchStats()
        let cellIndexPath = IndexPath(item: 0, section: 0)
        XCTAssertEqual(statsViewModel.cellCount(), 2)
        guard let cellViewModel = statsViewModel.statsViewModel(indexPath: cellIndexPath) else {
            XCTFail()
            return
        }
        XCTAssertNil(shapesOperationModel.deletedShape)
        statsViewModel.deleteCell(indexPath: cellIndexPath)
        XCTAssertEqual(shapesOperationModel.deletedShape, cellViewModel.shape)
    }
    
}
