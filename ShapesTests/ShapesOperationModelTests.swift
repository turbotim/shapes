//
//  ShapeOperationModelTests.swift
//  ShapesTests
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import Shapes

class ShapeOperationModelTests: XCTestCase {
    
    var shapesOperationModel = ShapesOperationModel(shapeSize: CGSize(width: 100, height: 100))
    
    override func setUp() {
        super.setUp()
        shapesOperationModel = ShapesOperationModel(shapeSize: CGSize(width: 100, height: 100))
        shapesOperationModel.update(canvasBounds: CGRect(x: 0, y: 0, width: 400, height: 400))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateShape() {
        XCTAssertEqual(shapesOperationModel.shapeModels.count, 0)
        shapesOperationModel.create(shape: .circle)
        XCTAssertEqual(shapesOperationModel.shapeModels.count, 1)
        let shapeModel = shapesOperationModel.shapeModels[0]
        XCTAssertEqual(shapeModel.shape, Shape.circle)
    }
    
    func testShapesCreatedHaveDifferentFrames() {
        shapesOperationModel.create(shape: .square)
        shapesOperationModel.create(shape: .square)
        let firstShape = shapesOperationModel.shapeModels[0]
        let secondShape = shapesOperationModel.shapeModels[1]
        XCTAssertNotEqual(firstShape.frame.origin.x, secondShape.frame.origin.x)
        XCTAssertNotEqual(firstShape.frame.origin.y, secondShape.frame.origin.y)
    }
    
    func testCanMoveShape() {
        shapesOperationModel.create(shape: .triangle)
        let initialShapeModel = shapesOperationModel.shapeModels[0]
        shapesOperationModel.move(shapeModel: initialShapeModel, toOrigin: CGPoint(x: 256, y: 222))
        let finalShapeModel = shapesOperationModel.shapeModels[0]
        XCTAssertNotEqual(initialShapeModel.frame.origin.x, finalShapeModel.frame.origin.x)
        XCTAssertNotEqual(initialShapeModel.frame.origin.y, finalShapeModel.frame.origin.y)
    }
    
    func testCanUndoMove() {
        shapesOperationModel.create(shape: .triangle)
        let initialShapeModel = shapesOperationModel.shapeModels[0]
        shapesOperationModel.move(shapeModel: initialShapeModel, toOrigin: CGPoint(x: 256, y: 222))
        let movedShapeModel = shapesOperationModel.shapeModels[0]
        shapesOperationModel.undoLastOperation()
        let finalShapeModel = shapesOperationModel.shapeModels[0]
        XCTAssertNotEqual(movedShapeModel.frame.origin.x, finalShapeModel.frame.origin.x)
        XCTAssertNotEqual(movedShapeModel.frame.origin.y, finalShapeModel.frame.origin.y)
        XCTAssertEqual(initialShapeModel.frame.origin.x, finalShapeModel.frame.origin.x)
        XCTAssertEqual(initialShapeModel.frame.origin.y, finalShapeModel.frame.origin.y)
    }
    
    func testNotifiesWhenShapeModelsUpdated() {
        let expectation = XCTestExpectation(description: "Shape model updates notifies")
        shapesOperationModel.shapeModelsUpdated = {
            expectation.fulfill()
        }
        shapesOperationModel.create(shape: .circle)
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testCountsOperations() {
        XCTAssertEqual(shapesOperationModel.operationCount(), 0)
        shapesOperationModel.create(shape: .circle)
        XCTAssertEqual(shapesOperationModel.operationCount(), 1)
        shapesOperationModel.move(shapeModel: shapesOperationModel.shapeModels[0], toOrigin: CGPoint(x: 0, y: 0))
        XCTAssertEqual(shapesOperationModel.operationCount(), 2)
        shapesOperationModel.undoLastOperation()
        XCTAssertEqual(shapesOperationModel.operationCount(), 1)
    }
    
    func testCanDeleteAll() {
        shapesOperationModel.create(shape: .circle)
        shapesOperationModel.create(shape: .circle)
        shapesOperationModel.create(shape: .triangle)
        XCTAssert(shapesOperationModel.shapeModels.contains { $0.shape == .circle })
        XCTAssert(shapesOperationModel.shapeModels.contains { $0.shape == .triangle })
        shapesOperationModel.deleteAll(shape: .circle)
        XCTAssertFalse(shapesOperationModel.shapeModels.contains { $0.shape == .circle })
        XCTAssert(shapesOperationModel.shapeModels.contains { $0.shape == .triangle })
    }
}
