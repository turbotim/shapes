//
//  ShapesOperationModelMock.swift
//  ShapesTests
//
//  Created by Tim Harrison on 02/02/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit
@testable import Shapes

class ShapesOperationModelMock: ShapesOperationModelable {
    var shapeModels: [ShapeModel] = [ShapeModel]()
    var shapeModelsUpdated: (() -> ())?
    var rawOperationCount = 0
    var deletedShape: Shape? = nil
    
    func changeModel() {
        shapeModelsUpdated?()
    }
    
    func update(canvasBounds: CGRect) {
        
    }
    
    func create(shape: Shape) {
        
    }
    
    func move(shapeModel: ShapeModel, toOrigin origin: CGPoint) {
        
    }
    
    func undoLastOperation() {
        
    }
    
    func deleteAll(shape: Shape) {
        deletedShape = shape
    }
    
    func delete(shapeModel: ShapeModel) {
        
    }
    
    func operationCount() -> Int {
        return rawOperationCount
    }
}
